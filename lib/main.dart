import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static const BATTERY_CHANNEL = "com.example.FlutterPlanel/battery";
  static const IMAGE_PICKER_CHANNEL = "com.example.FlutterPlanel/image-picker";

  static const batteryChannel = const MethodChannel(BATTERY_CHANNEL);
  static const imagePickerChannel = const MethodChannel(IMAGE_PICKER_CHANNEL);

  // state
  String _batteryLevel = "Unknown battery level.";
  String _imagePath;

  _MyHomePageState() {
    // Receive method invocations from platform and return results.
    batteryChannel.setMethodCallHandler((MethodCall call) async {
      switch (call.method) {
        case 'bar':
          return 'Hello, ${call.arguments}';
        case 'baz':
          throw PlatformException(code: '400', message: 'This is bad');
        default:
          throw MissingPluginException();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(widget.title),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
            ),
          ],
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ClipRRect(
              borderRadius: BorderRadius.circular(50),
              child: FlatButton(
                padding: EdgeInsets.all(0.0),
                onPressed: _pickImage,
                child: Container(
                  color: Colors.blueGrey[50],
                  width: 100,
                  height: 100,
                  child: _imagePath != null
                      ? Image.file(
                          File(_imagePath),
                        )
                      : null,
                ),
              ),
            ),
            SizedBox(height: 44),
            Text(_batteryLevel),
            RaisedButton(
              child: Text('Check Battery'),
              onPressed: _getBatteryLevel,
            ),
            RaisedButton(
              child: Text('Welcome'),
              onPressed: _showWelcome,
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await batteryChannel.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level at $result %.';
    } on PlatformException catch (e) {
      batteryLevel = 'Failed to get battery level: ${e.message}';
    }

    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  Future<void> _showWelcome() async {
    try {
      await batteryChannel.invokeMethod('showWelcome');
    } on PlatformException catch (e) {
      print(e.message);
    }
  }

  Future<void> _pickImage() async {
    String imagePath;
    try {
      imagePath = await imagePickerChannel.invokeMethod('pickImage');
      print("imagePath $imagePath");
    } catch (e) {
      print(e.message);
    }

    setState(() {
      _imagePath = imagePath ?? _imagePath;
    });
  }
}
