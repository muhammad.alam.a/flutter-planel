import UIKit
import Flutter
import SVProgressHUD

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    private let BATTERY_CHANNEL = "com.example.FlutterPlanel/battery"
    private let IMAGE_PICKER_CHANNEL = "com.example.FlutterPlanel/image-picker"
    
    override func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
        
        let batteryChannel = FlutterMethodChannel(name: BATTERY_CHANNEL, binaryMessenger: controller.binaryMessenger)
        batteryChannel.setMethodCallHandler { [weak self] call, result in
            if call.method == "getBatteryLevel" {
                self?.receiveBatteryLevel(result: result)
                SVProgressHUD.showInfo(withStatus: "This is john connor.")
            } else if call.method == "showWelcome" {
                self?.showWelcome(result: result)
            } else {
                result(FlutterMethodNotImplemented)
            }
        }
        
        let imagePickerChannel = FlutterMethodChannel(name: IMAGE_PICKER_CHANNEL, binaryMessenger: controller.binaryMessenger)
        imagePickerChannel.setMethodCallHandler { [weak self] call, result in
            if call.method == "pickImage" {
                self?.pickImage(result: result)
            } else {
                result(FlutterMethodNotImplemented)
            }
        }
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func receiveBatteryLevel(result: FlutterResult) {
        let device = UIDevice.current
        device.isBatteryMonitoringEnabled = true
        if device.batteryState == .unknown {
            result(FlutterError(code: "UNAVAILABLE", message: "Battery info unavailable", details: nil))
        } else {
            result(Int(device.batteryLevel * 100))
        }
    }
    
    private func showWelcome(result: FlutterResult) {
        let controller = WelcomeViewController.instantiateNav()
        let parent = window?.rootViewController
        parent?.present(controller, animated: true, completion: nil)
        result(true)
    }
    
    private func pickImage(result: @escaping FlutterResult) {
        let parent = window!.rootViewController!
        ImagePicker.show(parent, success: { imagePath in
            result(imagePath)
        }, failure: { error in
            result(FlutterError(code: "IMAGE_PICKER_FAILED", message: error.localizedDescription, details: nil))
        })
    }
    
}
