//
//  ImagePicker.swift
//  Runner
//
//  Created by Alam Akbar Muhammad on 28/08/20.
//

import Foundation
import YPImagePicker

class ImagePicker {
    
    typealias OnSuccess = (_ path: String) -> Void
    typealias OnFailure = (_ error: NSError) -> Void
    
    static func show(_ target: UIViewController, success: @escaping OnSuccess, failure: OnFailure? = nil) {
        let picker = YPImagePicker()
        picker.didFinishPicking { [unowned picker] items, _ in
            var imagePath: String?
            var pickerError: NSError?
            if let photo = items.singlePhoto {
                if let data = photo.image.jpegData(compressionQuality: 0.8) {
                    let filename = getDocumentsDirectory().appendingPathComponent("image_\(Date()).jpg")
                    do {
                        try data.write(to: filename)
                        imagePath = filename.path
                    } catch {
                        pickerError = error as NSError
                    }
                }
            }
            if let imagePath = imagePath {
                success(imagePath)
            } else {
                let error = pickerError ?? NSError(domain: "runner", code: 0, userInfo: [NSLocalizedDescriptionKey: "ImagePicker failed to pick image."])
                failure?(error)
            }
            picker.dismiss(animated: true, completion: nil)
        }
        target.present(picker, animated: true, completion: nil)
    }
    
    private static func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
}
