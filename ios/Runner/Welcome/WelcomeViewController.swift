//
//  WelcomeViewController.swift
//  Runner
//
//  Created by Alam Akbar Muhammad on 26/08/20.
//

import UIKit

class WelcomeViewController: UIViewController {

    static func instantiateNav() -> UINavigationController {
        let controller = WelcomeViewController()
        let nav = UINavigationController(rootViewController: controller)
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        navigationItem.title = "Welcome iOS"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(closeDidTap))
    }
    
    @objc private func closeDidTap(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
